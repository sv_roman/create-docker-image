FROM python:3.9.6

EXPOSE 8080

ADD . /app

RUN pip install -r /app/requirements.txt

ENTRYPOINT python /app/app.py

